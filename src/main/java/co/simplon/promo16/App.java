package co.simplon.promo16;

import java.util.Scanner;

public class App {
    protected static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        boolean isCorrect = false;
        System.out.println("game mode :");
        System.out.println("pour jouer seule entrer 'solo'");
        System.out.println("pour jouer à 2 entrer 'multi'");
        while (!isCorrect) {
            String gameMode = scanner.nextLine();
            if (gameMode.equals("solo")) {
                isCorrect = true;
                System.out.println("Entrez votre nom: ");
                String nom = scanner.nextLine();
                System.out.println("--");
                Partie p = new Partie(new Ordinateur(Jeu.BLEU), new Humain(nom, Jeu.ROUGE));
                p.joue();
            } else if (gameMode.equals("multi")) {
                isCorrect = true;
                System.out.println("Entrez nom du joueur 1 : ");
                String player1 = scanner.nextLine();
                System.out.println("Entrez nom du joueur 2 : ");
                String player2 = scanner.nextLine();
                System.out.println("--");
                Partie p = new Partie(new Humain(player1, Jeu.BLEU), new Humain(player2, Jeu.ROUGE));
                p.joue();
            } else {
                System.out.println("Erreur, ressaisissez");
            }
        }

    }
}
