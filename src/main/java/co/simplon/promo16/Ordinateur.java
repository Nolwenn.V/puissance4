package co.simplon.promo16;

import java.util.Random;
/*
* Classe qui hérite de la classe joueur
* Classe permettant de jouer face à un ordinateur qui joue aléatoirement
*/

public class Ordinateur extends Joueur {

    public Ordinateur(int couleur) {
        super("Le programme", couleur);
    }

    public void joue(Jeu jeu) {
        Random rand = new Random();
        int hasard = rand.nextInt(7);

        if (jeu.joueCoup(hasard, this.getCouleur())) {
            System.out.println(this.getNom() + " a joué en " + (hasard));
            return;
        }
    }
}
