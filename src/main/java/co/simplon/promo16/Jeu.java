package co.simplon.promo16;

public class Jeu {
  public final static int VIDE = 0;
  public final static int BLEU = 1;
  public final static int ROUGE = 2;
  final String ANSI_RED = "\u001B[31m";
  final String ANSI_BLUE = "\u001B[34m";
  final String ANSI_RESET = "\u001B[0m";
  String red = ANSI_RED + "O" + ANSI_RESET;
  String blue = ANSI_BLUE + "O" + ANSI_RESET;

  private int taille;
  private int[][] grille;

  public Jeu(int taille) {
    initJeu(taille);
  }

  public Jeu() {
    initJeu(8);
  }

  /*
   * Méthode permettant d'initialiser la grille de puissance 4
   */
  private void initJeu(int taille) {
    this.taille = taille;
    grille = new int[taille][taille];
    for (int col = 0; col < taille; col++) {
      for (int row = 0; row < taille; row++) {
        grille[col][row] = VIDE;
      }
    }
  }

  public boolean joueCoup(int col, int joueur) {
    if ((col < 0) || (col >= taille)) {
      return false;
    }

    for (int ligne = 0; ligne < taille; ligne++) {
      if (grille[col][ligne] == VIDE) {
        grille[col][ligne] = joueur;
        return true;
      }
    }

    return false;
  }

  /*
   * Methode permettant de rechercher quatre jetons aligner
   */
  public boolean cherche4() {

    for (int ligne = 0; ligne < taille; ligne++) {
      if (cherche4alignes(0, ligne, 1, 0)) {
        return true;
      }
    }

    for (int col = 0; col < taille; col++) {
      if (cherche4alignes(col, 0, 0, 1)) {
        return true;
      }
    }

    for (int col = 0; col < taille; col++) {

      if (cherche4alignes(col, 0, 1, 1)) {
        return true;
      }

      if (cherche4alignes(col, 0, -1, 1)) {
        return true;
      }
    }

    for (int ligne = 0; ligne < taille; ligne++) {

      if (cherche4alignes(0, ligne, 1, 1)) {
        return true;
      }

      if (cherche4alignes(taille - 1, ligne, -1, 1)) {
        return true;
      }
    }

    return false;
  }

  private boolean cherche4alignes(int oCol, int oLigne, int dCol, int dLigne) {
    int couleur = VIDE;
    int compteur = 0;

    int curCol = oCol;
    int curRow = oLigne;

    while ((curCol >= 0) && (curCol < taille) && (curRow >= 0) && (curRow < taille)) {
      if (grille[curRow][curCol] != couleur) {

        couleur = grille[curRow][curCol];
        compteur = 1;
      } else {

        compteur++;
      }

      if ((couleur != VIDE) && (compteur == 4)) {
        return true;
      }

      curCol += dCol;
      curRow += dLigne;
    }

    return false;
  }

  public boolean estPlein() {

    for (int col = 0; col < taille; col++) {
      for (int ligne = 0; ligne < taille; ligne++) {
        if (grille[col][ligne] == VIDE) {
          return false;
        }
      }
    }

    return true;
  }

  public int getTaille() {
    return taille;
  }

  /*
   * Methode qui permet d'afficher la grille de puissance 4
   */
  public void afficher() {

    for (int ligne = taille - 1; ligne >= 0; --ligne) {
      for (int col = 0; col < taille; col++) {
        System.out.print("|");
        switch (grille[col][ligne]) {
          case VIDE:
            System.out.print("   ");
            break;
          case ROUGE:
            System.out.print(" " + red + " ");
            break;
          case BLEU:
            System.out.print(" " + blue + " ");
            break;
        }

      }
      System.out.print("|");
      System.out.println();
    }

    for (int i = 1; i <= taille; ++i) {
      System.out.print("  " + i + " ");
    }
    System.out.println();
  }

}
