package co.simplon.promo16;

public class Humain extends Joueur {
    private int col;

    public Humain(String nom, int couleur) {
        super(nom, couleur);
    }

    /*
     * Methode qui affiche ce que le joueur doit faire
     */
    public void joue(Jeu jeu) {
        jeu.afficher();

        boolean valide;
        do {
            System.out.println("Joueur " + this.getNom() + ", entrez un numéro de colonne" +
                    " entre 1 et " + jeu.getTaille() + ": ");

            col = checkInputType(App.scanner.nextLine());
            valide = jeu.joueCoup(col, this.getCouleur());
            if (valide == false) {
                System.out.println("-> Coup NON valide.");
            }
        } while (valide == false);
    }

    public int checkInputType(String input) {
        int inputNumber = 0;
        try {
            inputNumber = Integer.parseInt(input);
        } catch (NumberFormatException ex) {
        }
        return inputNumber - 1;
    }

}
