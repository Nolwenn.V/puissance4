package co.simplon.promo16;

public class Partie {
    private Joueur[] joueurs = new Joueur[2];
    private Jeu jeu;

    /*
     * Methode qui permet d'initialiser la partie
     */
    public Partie(Joueur joueur1, Joueur joueur2) {
        joueurs[0] = joueur1;
        joueurs[1] = joueur2;
        jeu = new Jeu();
    }

    /*
     * Methode qui permet de jouer un coup
     */
    public void joue() {
        int vainqueur = -1;
        int cJoueur = 0;

        while (vainqueur == -1) {
            joueurs[cJoueur].joue(jeu);
            if (jeu.estPlein()) {
                vainqueur = -1;
            }

            if (jeu.cherche4()) {
                vainqueur = cJoueur;
            }

            cJoueur++;
            cJoueur %= 2;
        }

        System.out.println("La partie est finie.");
        jeu.afficher();
        if (vainqueur == -1) {
            System.out.println("Match nul.");
        } else {
            System.out.println("Le vainqueur est " + joueurs[vainqueur].getNom());
        }
    }

}
